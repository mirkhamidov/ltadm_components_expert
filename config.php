<?
$config=array(
  "name" => "Мнения экспертов",
  "status" => "system",
  "windows" => array(
                      "create" => array("width" => 1000,"height" => 700),
                      "edit" => array("width" => 550,"height" => 400),
                       "list" => array("width" => 550,"height" => 500),
                    ),
  "right" => array("admin","#GRANTED"),
  "main_table" => "lt_expert",
  "list" => array(
    "content" => array("isLink" => true),
    "unixtime" => array("align" => "center"),
  ),
  "select" => array(
     "max_perpage" => 20,
     "default_orders" => array(
                           array("unixtime" => "DESC"),
                         ),
     "default" => array(
        "day" => array(
         "desc" => "День",
         "type" => "select_simple",
         "values" => array(
           ''=>'',
           '1'=>'1',
           '2'=>'2',
           '3'=>'3',
           '4'=>'4',
           '5'=>'5',
           '6'=>'6',
           '7'=>'7',
           '8'=>'8',
           '9'=>'9',
           '10'=>'10',
           '11'=>'11',
           '12'=>'12',
           '13'=>'13',
           '14'=>'14',
           '15'=>'15',
           '16'=>'16',
           '17'=>'17',
           '18'=>'18',
           '19'=>'19',
           '20'=>'20',
           '21'=>'21',
           '22'=>'22',
           '23'=>'23',
           '24'=>'24',
           '25'=>'25',
           '26'=>'26',
           '27'=>'27',
           '28'=>'28',
           '29'=>'29',
           '30'=>'30',
           '31'=>'31',
         ),
       ),
        "month" => array(
         "desc" => "Месяц",
         "type" => "select_simple",
         "values" => array(
           ''=>'',
           '1'=>'1',
           '2'=>'2',
           '3'=>'3',
           '4'=>'4',
           '5'=>'5',
           '6'=>'6',
           '7'=>'7',
           '8'=>'8',
           '9'=>'9',
           '10'=>'10',
           '11'=>'11',
           '12'=>'12',
         ),
       ),
       "year" => array(
              "desc" => "Год",
              "type" => "text",
              "maxlength" => "4",
              "size" => "4",
              "value" => date("Y"),
         ),

     ),
  ),
);
$actions=array(
  "create" => array(
    "before_code" => "create.php",
  ),
  "edit" => array(
    "before_code" => "edit.php",
  ),
);

?>